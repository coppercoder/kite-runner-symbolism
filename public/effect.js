function curve(pct) {
  return Math.sin(Math.PI * pct / 2);
}

// an RNG - https://stackoverflow.com/a/47593316
function mulberry32(a) {
  return function() {
    var t = a += 0x6D2B79F5;
    t = Math.imul(t ^ t >>> 15, t | 1);
    t ^= t + Math.imul(t ^ t >>> 7, t | 61);
    return ((t ^ t >>> 14) >>> 0) / 4294967296;
  }
}

// shuffles array with Fisher-Yates - https://stackoverflow.com/a/6274381/8468108
function shuffle(a, rng) {
  var j, x, i;
  for (i = a.length - 1; i > 0; i--) {
      j = Math.floor(rng() * (i + 1));
      x = a[i];
      a[i] = a[j];
      a[j] = x;
  }
  return a;
}

// global variables but whatever, this is a small project
let SYMBOLS = [
  'Б', 'б', 'Г', 'д', 'Ж', 'З', 'й', 'К', 'л', 'П', 'ф', 'У', 'ц',
  'Ч', 'ш', 'Ъ', 'ы', 'Ь', 'э', 'Ю', 'я', 'ζ', 'Σ', 'Ψ', 'χ', 'Ξ',
  'Δ', 'Θ', 'π'
];
let orderToScramble = new Array();
let originalText;

// generate an order to replace the symbols
function setupTextScrambling() {
  originalText = $('#text-scrambling').text();
  for (var i = 0; i < originalText.length; i++) {
    orderToScramble[i] = i;
  }
  
  let rng = mulberry32(1092749211523518);
  shuffle(orderToScramble, rng);
}

let creepyTextAnim;
var creepyTextWritten = false;

let handwritingAnim;
var handwritingWritten = false;

function resetCreepyText() {
  $('#creepy-text').html(''); // clear creepy text
  creepyTextAnim = new Vara('#creepy-text', 'font/exquisite-corpse.json', [{
    text: 'What does he know, that illiterate Hazara?',
    fontSize: 50,
    duration: 3000
  }, {
    text: 'How dare he criticize you?',
    fontSize: 65,
    duration: 4000
  }], {
    strokeWidth: 0.5,
    textAlign: 'center',
    autoAnimation: false,
    letterSpacing: 2,
    color: 'white'
  });
}

function resetHandwriting() {
  $('#handwriting').html(''); // clear
  handwritingAnim = new Vara('#handwriting', 'font/parisienne.json', [{
    text: 'His son was not going to grow up illiterate like he had.',
    fontSize: 35,
    duration: 5000,
    color: 'black',
    autoAnimation: false,
    strokeWidth: 0.7
  }]);
}

var powerCycleBroken = false;
var quote6Animated = false;
var quote6Animating = false;

function triggerAnimation(selector, remove, add) {
  $(selector).removeClass(remove)
    .css('animation', 'none');
  document.querySelector(selector).offsetHeight;
  $(selector).css('animation', '')
    .addClass(add);
}


// start/stop scroll events - https://gist.github.com/timhudson/5484248
var lastScrollAt = Date.now();
var timeout;

function scrollStartStop() {
    var $this = $(this);
    if (Date.now() - lastScrollAt > 100) {
        $this.trigger('scrollstart');
    }
    lastScrollAt = Date.now();
    clearTimeout(timeout);
    timeout = setTimeout(function() {
        if (Date.now() - lastScrollAt > 99) {
          $this.trigger('scrollend');
        }
    }, 100);
}
$(document).on('scroll', scrollStartStop);

// play quote 4 gif while scrolling
var quote4gif;

$(document).ready(function() {
  $('.first-page').css('margin-top', $(window).height() + 'px');
  $('.page').css('margin-bottom', ($(window).height()*1.1) + 'px');
  
  setupTextScrambling();
  resetCreepyText();
  resetHandwriting();
  
  quote4gif = new SuperGif({gif: document.querySelector('#quote-4-graphic')})
  quote4gif.load();
  $(document).on('scrollstart', function() {setTimeout(quote4gif.play, 10)});
  $(document).on('scrollend', function() {setTimeout(quote4gif.pause, 10)});
});

function updateBackground(id, fracScrolled, background) {
  let filter = '';
  
  if (id === 'quote-6') {
    if (!quote6Animating) {
      if (quote6Animated) {
        $('#background').css('background-image', 'url(./quote-6-img-2.jpg)');
        filter = 'brightness(0.5) sepia(50%)';
      } else {
        $('#background').css('background-image', 'url(./quote-6-img-1.jpg)');
      }
    }
  } else {
    $('#background').css('background-image', background)
      .removeClass('transition-fwd')
      .removeClass('transition-rev');
  }
  
  let oFilter = 'opacity(1)';
  if (fracScrolled < 0.1) {
    // 0% to 10% => fade in scene
    oFilter = 'opacity(' + Math.max(fracScrolled / 0.1, 0) + ')';
  } else if (fracScrolled >= 0.1 && fracScrolled < 0.25) {
    // fade in vids
    $('.vid').css('opacity', ((fracScrolled - 0.1) / 0.15));
  } else if (fracScrolled >= 0.25 && fracScrolled < 0.9) {
    $('.vid').css('opacity', 1.0);
  } else if (fracScrolled >= 0.9) {
    // 90% to 100% => fade out scene
    let opacity = (1 - ((fracScrolled - 0.9) / 0.1));
    oFilter = 'opacity(' + opacity + ')';
    $('.vid').css('opacity', opacity);
  }
  
  filter += ' ' + oFilter;
  $('#background').css({'-webkit-filter': filter, 'filter': filter});
}

$(window).scroll(function(event) {
  let scrollTop = $(window).scrollTop();
  $('.page').each(function() {
    let y = scrollTop - $(this).offset().top + $(window).height();
    let height = $(this).height() - 4;
    if (y < 0 || y > $(window).height() + height) return;
    
    let fracScrolled = y / ($(window).height() + height);
    
    let id = $(this)[0].id;
    
    updateBackground(id, fracScrolled, $(this).attr('data-background'));
    
    // quote 6 background transition
    if (id === 'quote-6') {
      var animating = false;
      if (quote6Animated && fracScrolled < 0.3) {
        triggerAnimation('#background', 'transition-fwd', 'transition-rev');
        animating = true;
        quote6Animated = false;
      } else if (!quote6Animated && fracScrolled > 0.3) {
        triggerAnimation('#background', 'transition-rev', 'transition-fwd');
        animating = true;
        quote6Animated = true;
      }
      if (animating) {
        quote6Animating = true;
        setTimeout(function() {
          quote6Animating = false;
          $('#background').removeClass('transition-fwd').removeClass('transition-rev');
          updateBackground(id, fracScrolled, '');
        }, 2000);
      }
    }
    
    // fade in X on ABC in quote 1
    if (fracScrolled >= 0.3 && fracScrolled < 0.55 && id === 'quote-1') {
      let through = (fracScrolled - 0.3) / 0.25;
      let transformed = 1 - curve(through);
      $('#red-x').css('opacity', through * 0.8) // max at 80% opaque
        .css('margin-top', transformed * -$('#red-x').height()*2)
        .css('margin-left', transformed * -$('#red-x').width()*2);
    } else if (fracScrolled < 0.2) {
      $('#red-x').css('opacity', 0);
    } else if (fracScrolled >= 0.45) {
      $('#red-x').css('opacity', 0.8);
    }
    
    // scramble the text in quote 2
    if (id === 'quote-2') {
      if (fracScrolled < 0.55) {
        $('#text-scrambling').text(originalText);
      } else {
        let through = Math.min((fracScrolled - 0.55) / 0.4, 1);
        let pctSymbol = curve(through);
        let numSymbol = Math.ceil(pctSymbol * originalText.length);
        let rng = mulberry32(1982462897);
        var newText = originalText + '';
        for (var i = 0; i < numSymbol; i++) {
          let idx = orderToScramble[i];
          if (/^\s$/.test(newText[idx])) continue;
          let symbol = SYMBOLS[Math.floor(rng() * SYMBOLS.length)];
          newText = newText.substring(0, idx) + symbol + newText.substring(idx + 1);
        }
        $('#text-scrambling').text(newText);
      }
    }
    
    // creepy text with Vara in quote 3
    if (id === 'quote-3') {
      if (fracScrolled > 0.4 && fracScrolled < 0.6 && !creepyTextWritten) {
        creepyTextAnim.playAll();
        creepyTextWritten = true;
      }
    } else if (creepyTextWritten) {
      resetCreepyText();
      creepyTextWritten = false;
    }
    
    // power cycle breaking thing
    if (id === 'timejump') {
      if (powerCycleBroken && fracScrolled < 0.5) {
        triggerAnimation('#cycle-left-half', 'animate-cycle-left-fwd', 'animate-cycle-left-rev');
        triggerAnimation('#cycle-right-half', 'animate-cycle-right-fwd', 'animate-cycle-right-rev');
        triggerAnimation('#cycle-centre', 'animate-cycle-centre-fwd', 'animate-cycle-centre-rev');
        powerCycleBroken = false;
      } else if (!powerCycleBroken && fracScrolled > 0.5) {
        triggerAnimation('#cycle-left-half', 'animate-cycle-left-rev', 'animate-cycle-left-fwd');
        triggerAnimation('#cycle-right-half', 'animate-cycle-right-rev', 'animate-cycle-right-fwd');
        triggerAnimation('#cycle-centre', 'animate-cycle-centre-rev', 'animate-cycle-centre-fwd');
        powerCycleBroken = true;
      }
    }
    
    // fade in/out book gif
    if (id === 'quote-4') {
      if (fracScrolled < 0.1) {
        filter = 'opacity(' + (fracScrolled / 0.1) + ')';
      } else if (fracScrolled >= 0.1 && fracScrolled < 0.9) {
        filter = 'opacity(1)';
      } else if (fracScrolled >= 0.9) {
        filter = 'opacity(' + ((1 - fracScrolled) / 0.1) + ')';
      }
      $('.jsgif').css({'-webkit-filter': filter, 'filter': filter});
      $('#quote-4-graphic').css({'-webkit-filter': filter, 'filter': filter});
    }
    
    // handwriting with Vara in quote 5
    if (id === 'quote-5') {
      if (fracScrolled > 0.4 && fracScrolled < 0.6 && !handwritingWritten) {
        handwritingAnim.playAll();
        handwritingWritten = true;
      }
      
      // fade in/out paper
      if (fracScrolled < 0.1) {
        $('#writing-text').css('opacity', fracScrolled / 0.1);
      } else if (fracScrolled >= 0.1 && fracScrolled < 0.9) {
        $('#writing-text').css('opacity', 1);
      } else if (fracScrolled >= 0.9) {
        $('#writing-text').css('opacity', (1 - fracScrolled) / 0.1);
      }
    } else if (handwritingWritten) {
      resetHandwriting();
      handwritingWritten = false;
    }
  });
});

function revealThesis() {
  $('#reveal-thesis').fadeOut(400);
  $('#thesis-text').fadeIn(2000);
}
